package chessPieces;

import chess.Board;

public abstract class Queen extends ChessPiece{

	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	
	public String write(){
		return "pw";
	}

	
	@Override
	public abstract String getPlayer();

	
	
	/**This method implements the specific move of the piece.
	 * @param dx location of the destination of the piece to move
	 * @param dy location of the destination of the piece to move
	 * @param board board in which the game is being played on
	 * @return  a board after the move has been implemented
	 */
	@Override
	public Board move(int dx, int dy, Board board){
		WhiteBishop b = new WhiteBishop();
		b.x=this.x;
		b.y=this.y;
		board = b.move(dx, dy, board);
		
	    if(board.board[x][y]==null){
	    	return board;
	    }else{
	    	  WhiteRock r = new WhiteRock();
	    	  r.x=this.x;
	    	  r.y=this.y;
	    	  board = r.move(dx, dy, board);
	    	  if(board.board[x][y]==null){

	    		  return board;
	    	  }else{
	    		  return board;
	    	  }
	      }
	    }
		
	

}
