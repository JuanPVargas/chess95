package chessPieces;

import chess.Board;
import chess.Chess;

public abstract class King extends ChessPiece{

	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	
	public abstract String write();
	
	public Board move(int dx, int dy, Board board){
        //castling move
		if(dy>y && dy-y==2 && board.board[dx][dy+1]instanceof Rock && board.board[dx][dy]==null
				&& board.board[x][dy-1]==null && dx==x){
			board.board[dx][dy]=board.board[x][y];
			board.board[x][y]=null;
			board.board[dx][dy-1]=board.board[dx][dy+1];
			board.board[dx][dy+1]=null;				    
		}else if(y>dy && y-dy==2 && board.board[x][dy-2]instanceof Rock && board.board[dx][dy]==null
				&& board.board[x][dy-1]==null && board.board[dx][dy+1]==null && x==dx){
			board.board[dx][dy]=board.board[x][y];
			board.board[x][y]=null;
			board.board[dx][dy+1]=board.board[dx][dy-2];
			board.board[dx][dy-2]=null;	
		}
				
		//System.out.println("-----");
		if((Math.abs(dx-x)==1 && dy==y )  ||  (dx==x && Math.abs(dy-y)==1)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
     	  }else{
               if(Chess.EmpDanger(board,dx,dy,x,y)){
            	   return board;
               }else{
     		       board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
               }
     	  }
			
		}else{

			return board;
		}
		
	}
}
