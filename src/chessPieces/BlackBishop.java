package chessPieces;



public class BlackBishop extends Bishop{
 
	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	
	
	public String write(){
		return "bB";
	}
	
	@Override
	public String getPlayer() {
		return "black";
	}

	
}
