package chessPieces;

import chess.Board;

public abstract class Bishop extends ChessPiece{

	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	
	
	public abstract String write();

	@Override
	public abstract String getPlayer();
	
	/**This method implements the specific move of the piece.
	 * @param dx location of the destination of the piece to move
	 * @param dy location of the destination of the piece to move
	 * @param board board in which the game is being played on
	 * @return  a board after the move has been implemented
	 */
	@Override
	public Board move(int dx, int dy, Board board){
    	//System.out.println("::"+""+x+""+y+" "+dx+""+dy);

          if(dx<x&&dy<y){
        	  for(int i=1;i<(x-dx);i++){
        		  if(board.board[x-i][y-i]!=null ){
        			  return board;
        		  }else if((y-i)<dy){
        			  return board;
        		  }else if((x-i)<dx){
        			  return board;
        		  }
        	  }
             
        	  if(board.board[dx][dy]!=null){
        		  if(x-dx != y-dy){
                	  return board;
                  }
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
        	  }else{
        		  if(x-dx != y-dy){
                	  return board;
                  }
        		  board.board[dx][dy] = board.board[x][y];
				  board.board[x][y] = null;
				  return board;
        	  }
        	  
          }else if(dx<x&&dy>y){

        	  for(int i=1;i<(x-dx);i++){
        		  if(board.board[x-i][y+i]!=null ){
        			  return board;
        		  }else if(y+i>dy){
        			  return board;
        		  }else if((x-i)<dx){
        			  return board;
        		  }
        	  }
             
        	  if(board.board[dx][dy]!=null){
        		  if(Math.abs(dx-x) != Math.abs(dy-y)){
                	  return board;
                  }
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
        	  }else{
        		  if(Math.abs(dx-x) != Math.abs(dy-y)){
                	  return board;
                  }
        		   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
        	  }
        	  
          }else if(dx>x&&dy<y){
        	  for(int i=1;i<(dx-x);i++){
        		  if(board.board[x+i][y-i]!=null){
        			  return board;
        		  }else if((y-i)<dy){
        			  return board;
        		  }else if((x+i)>dx){
        			  return board;
        		  }
        	  }
             
        	  if(board.board[dx][dy]!=null){
        		  if(Math.abs(dx-x) != Math.abs(dy-y)){
                	  return board;
                  }
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
        	  }else{
        		  if(Math.abs(dx-x) != Math.abs(dy-y)){
                	  return board;
                  }
        		   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
        	  }
        	  
          }else if(dx>x&&dy>y){
        	  for(int i=1;i<(dx-x);i++){
        		  if(board.board[x+i][y+i]!=null){
        			  return board;
        		  }else if((y+i)>dy){
        			  return board;
        		  }else if((x+i)>dx){
        			  return board;
        		  }
        	  }              
        	          	  
        	  if(board.board[dx][dy]!=null){
        		  if(dx-x != dy-y){
                	  return board;
                  }

				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
        	  }else{
        		  if(dx-x != dy-y ){
                	  return board;
                  }
          		  //System.out.println("..");
        		   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
        	  }
          }else{
        	  //System.out.println("..");
        	  return board;
          }
           
		
	}
	
}
