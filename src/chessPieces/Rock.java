package chessPieces;

import chess.Board;

public abstract class Rock extends ChessPiece{

	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	/**
	 * abstarct mehtod that used by subclass returns the especific piece name.
	 */
	public abstract String write();
    
	/**This method implements the specific move of the piece.
	 * @param dx location of the destination of the piece to move
	 * @param dy location of the destination of the piece to move
	 * @param board board in which the game is being played on
	 * @return  a board after the move has been implemented
	 */
	@Override
	public Board move(int dx, int dy, Board board) {
		      
			if(dy==y){
			    if(dx>x){
				   for(int i=1;i<(dx-x);i++){
					   if(board.board[x+i][dy]!=null){
						   return board;
					   }
				   }
				   if(board.board[dx][dy]!=null){
					   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
						   System.out.println("Cant kill your own team");
						   return board;
					   }else{
						   board.board[dx][dy] = board.board[x][y];
						   board.board[x][y] = null;
						   return board;
					   }
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
				      
			    }else{
			    	for(int i=1;i<(x-dx);i++){
						   if(board.board[x-i][dy]!=null){
							   return board;
						   }
					   }
					   if(board.board[dx][dy]!=null){
						   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
							   System.out.println("Cant kill your own team");
							   return board;
						   }else{
							   board.board[dx][dy] = board.board[x][y];
							   board.board[x][y] = null;
							   return board;
						   }
					   }else{
						   board.board[dx][dy] = board.board[x][y];
						   board.board[x][y] = null;
						   return board;
					   }
			    }
			}else if(dx==x){
				if(dy>y){	
					 for(int i=1;i<(dy-y);i++){
						   if(board.board[dx][y+i]!=null){
							   return board;
						   }
					   }
					   
					 if(board.board[dx][dy]!=null){
						   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
							   System.out.println("Cant kill your own team");
							   return board;
						   }else{
							   board.board[dx][dy] = board.board[x][y];
							   board.board[x][y] = null;
							   return board;
						   }
					   }else{
						   board.board[dx][dy] = board.board[x][y];
						   board.board[x][y] = null;
						   return board;
					   }			
				}else{
					for(int i=1;i<(y-dy);i++){
						   if(board.board[dx][y-i]!=null){
							   return board;
						   }
					   }
					   
					 if(board.board[dx][dy]!=null){
						   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
							   System.out.println("Cant kill your own team");
							   return board;
						   }else{
							   board.board[dx][dy] = board.board[x][y];
							   board.board[x][y] = null;
							   return board;
						   }
					   }else{
						   board.board[dx][dy] = board.board[x][y];
						   board.board[x][y] = null;
						   return board;
					   }
				      
				}
			}else{
				return board;
			}
          }	
		

	@Override
	public abstract String getPlayer();
}
