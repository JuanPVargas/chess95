package chessPieces;

import chess.Board;

public abstract class ChessPiece {

	
	
	public int x;
	public int y;
	
	public abstract String getPlayer();
	
	public abstract String write();
		
	public abstract Board move(int x, int y, Board b);
	
}
