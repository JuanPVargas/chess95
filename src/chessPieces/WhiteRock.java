package chessPieces;


public class WhiteRock extends Rock{

	
	public String write(){
		return "wR";
	}

	@Override
	public String getPlayer() {
		return "white";
	}

	
}
