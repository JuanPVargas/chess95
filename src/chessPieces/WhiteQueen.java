package chessPieces;


public class WhiteQueen extends Queen{
	
	
	public String write(){
		return "wQ";
	}

	@Override
	public String getPlayer() {
		return "white";
	}

	
}
