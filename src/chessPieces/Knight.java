package chessPieces;

import chess.Board;

public abstract class  Knight extends ChessPiece{

	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	
	public abstract String write();
	
	@Override
	public abstract String getPlayer();
	
	/**This method implements the specific move of the piece.
	 * @param dx location of the destination of the piece to move
	 * @param dy location of the destination of the piece to move
	 * @param board board in which the game is bein played on
	 * @return  a board after the move has been implemented
	 */
	public Board move(int dx, int dy, Board board){
		
		if( dx==(x-1) && dy == (y-2)){
			   if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
			
			
		}else if(dx==(x-2)&&dy==(y-1)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }  
			
		}else if(dx==(x-2)&&dy==(y+1)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
		}else if(dx==(x-1)&&dy==(y+2)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
		}else if(dx==(x+1)&&dy==(y-2)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
		}else if(dx==(x+2)&&dy==(y-1)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
		}else if(dx==(x+2)&&dy==(y+1)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
		}else if(dx==(x+1)&&dy==(y+2)){
			if(board.board[dx][dy]!=null){
				   if(board.board[dx][dy].getPlayer()==board.board[x][y].getPlayer()){
					   System.out.println("Cant kill your own team");
					   return board;
				   }else{
					   board.board[dx][dy] = board.board[x][y];
					   board.board[x][y] = null;
					   return board;
				   }
			   }else{
				   board.board[dx][dy] = board.board[x][y];
				   board.board[x][y] = null;
				   return board;
			   }
		}
		
		
		
		
		return board;
	}
}
