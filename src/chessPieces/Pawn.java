package chessPieces;
import chess.Board;


public abstract class Pawn extends ChessPiece{
   	
	/**	 
	 * @author Juan Vargas
	 * @author Steve Soltys
	 *         
	 */
	
	int state = 0;
	
	public abstract String write();
	
	/**This method implements the specific move of the piece.
	 * @param dx location of the destination of the piece to move
	 * @param dy location of the destination of the piece to move
	 * @param board board in which the game is being played on
	 * @return  a board after the move has been implemented
	 */
	public Board move(int dx, int dy, Board board){
         
		if(board.board[x][y].getPlayer().equals("white")){

			if(state == 0){			
				if((y==dy && x-dx==2) || (y==dy && x-dx==1)){
					board.board[dx][dy]=board.board[x][y];
					board.board[x][y]=null;
					state++;
					return board;
				}else if((x-dx==1) && (y-dy==1 || dy-y==1) && (board.board[dx][dy] != null) && board.board[dx][dy].getPlayer().equals("black") ){
					board.board[dx][dy]=board.board[x][y];
					board.board[x][y]=null;
					return board;
				}else{
					return board;
				}
				
			}else{		
				if(y==dy && x-dx==1){
					if(board.board[dx][dy]!= null){
						board.board[dx][dy]=board.board[x][y];
						board.board[x][y]=null;
					    if(dx==0){
					    	board.board[dx][dy]=null;
					    	board.board[dx][dy]=new WhiteQueen();
					    }
						return board;
					}else if(board.board[dx][dy]== null){
						board.board[dx][dy]=board.board[x][y];
						board.board[x][y]=null;
						if(dx==0){
					    	board.board[dx][dy]=null;
					    	board.board[dx][dy]=new WhiteQueen();
					    }
						return board;
					}
				}else if((x-dx==1) && (y-dy==1 || dy-y==1) && (board.board[dx][dy] != null) && board.board[dx][dy].getPlayer().equals("black")){
					board.board[dx][dy]=board.board[x][y];
					board.board[x][y]=null;
					if(dx==0){
				    	board.board[dx][dy]=null;
				    	board.board[dx][dy]=new WhiteQueen();
				    }
					return board;
				}
			}

			return board;
		}else if(board.board[x][y].getPlayer().equals("black")){
			if(state == 0){

				if((y==dy && dx-x==2)|| (y==dy && dx-x==1)){
					board.board[dx][dy]=board.board[x][y];
					board.board[x][y]=null;
					state++;
					return board;
				}else if((dx-x==1) && (y-dy==1 || dy-y==1) && (board.board[dx][dy] != null) && board.board[dx][dy].getPlayer().equals("white") ){
					board.board[dx][dy]=board.board[x][y];
					board.board[x][y]=null;
					return board;
				}else{
					return board;
				}
				
			}else{		
				if(y==dy && dx-x==1){
					if(board.board[dx][dy]!= null){
						board.board[dx][dy]=board.board[x][y];
						board.board[x][y]=null;
						if(dx==7){
					    	board.board[dx][dy]=null;
					    	board.board[dx][dy]=new BlackQueen();
					    }
						return board;
					}else if(board.board[dx][dy]== null){
						board.board[dx][dy]=board.board[x][y];
						board.board[x][y]=null;
						if(dx==7){
					    	board.board[dx][dy]=null;
					    	board.board[dx][dy]=new BlackQueen();
					    }
						return board;
					}
				}else if((dx-x==1) && (y-dy==1 || dy-y==1) && (board.board[dx][dy] != null)&& board.board[dx][dy].getPlayer().equals("white") ){
					board.board[dx][dy]=board.board[x][y];
					board.board[x][y]=null;
					if(dx==7){
				    	board.board[dx][dy]=null;
				    	board.board[dx][dy]=new BlackQueen();
				    }
					return board;
				}
			}

			return board;
		}

		return board;
	}

	@Override
	public abstract String getPlayer();
	
		
	
	
}
