package chess;

import chessPieces.*;


public class Board {

    /**
     * @author Juan Vargas
     * @author Steve Soltys
     *
     */

    /**
     * double array that contains the pieces of the chess game
     */
    public ChessPiece[][] board = new ChessPiece[8][8];

    /**
     * Board constructor
     */

    public Board() {
        board[0][0] = new BlackRock();
        board[0][1] = new BlackKnight();
        board[0][2] = new BlackBishop();
        board[0][3] = new BlackQueen();
        board[0][4] = new BlackKing();
        board[0][5] = new BlackBishop();
        board[0][6] = new BlackKnight();
        board[0][7] = new BlackRock();

        for (int i = 1; i <= 6; i++) {
            for (int j = 0; j < 8; j++) {

                board[1][j] = new BlackPawn();

                if (i == 6) {
                    board[6][j] = new WhitePawn();

                } else {
                    board[1][j] = null;
                }
            }
        }
        board[7][0] = new WhiteRock();
        board[7][1] = new WhiteKnight();
        board[7][2] = new WhiteBishop();
        board[7][3] = new WhiteQueen();
        board[7][4] = new WhiteKing();
        board[7][5] = new WhiteBishop();
        board[7][6] = new WhiteKnight();
        board[7][7] = new WhiteRock();

    }

    /**
     * graphBoard method
     * <p>
     * This method graphs the chess game board with the respective indications
     * <p>
     *
     * @param b board to be graphed
     */
    static void graphBoard(Board b) {
        System.out.println();
        for (int i = 0; i < b.board.length; i++) {
            for (int j = 0; j < b.board[0].length; j++) {
                ChessPiece piece = b.board[i][j];
                if (piece == null) {
                    if (i % 2 == 0) {
                        if ((i % 2 == 0 && j % 2 != 0)) {
                            if (j == 7) {
                                System.out.print("## " + (8 - i));
                            } else {
                                System.out.print("## ");
                            }
                        } else {
                            System.out.print("   ");
                        }
                    } else {
                        if ((i % 2 != 0 && j % 2 == 0)) {
                            System.out.print("## ");
                        } else {
                            if (j == 7) {
                                System.out.print("   " + (8 - i));
                            } else {
                                System.out.print("   ");
                            }
                        }
                    }
                } else {
                    b.board[i][j].x = i;
                    b.board[i][j].y = j;
                    if (j == 7) {
                        System.out.print(piece.write() + " " + (8 - i));
                    } else {
                        System.out.print(piece.write() + " ");
                    }
                }
            }
            System.out.println();
        }
        System.out.println("a  b  c  d  e  f  g  h");


    }


}
