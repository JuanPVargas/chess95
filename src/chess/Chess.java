package chess;

import chessPieces.*;

import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.Scanner;

/**
 * This class implements the chess game.
 *
 * @author Juan Vargas
 * @author Steve Soltys
 */
public class Chess {


    /**
     * promoPawn method
     * <p>
     * This method takes care of the promotion of pawns to an specific piece.
     * <p>
     *
     * @param input gets the input from the startGame,
     * @param board gets the board in which the game is being played on
     * @param x     postion of the pawn
     * @param y     postion of the pawn
     * @param dx    postion of the last row on the opposite player
     * @param dy    postion of the last row on the opposite player
     * @return return true if the promotion is done.
     */

    static boolean promoPawn(String input, Board board, int x, int y, int dx, int dy) {
        board = board.board[x][y].move(dx, dy, board);
        if (board.board[x][y] == null) {

            if ((int) input.charAt(6) == 'Q' && board.board[dx][dy] instanceof WhiteQueen) {
                board.board[dx][dy] = new WhiteQueen();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'B' && board.board[dx][dy] instanceof WhiteQueen) {
                board.board[dx][dy] = new WhiteBishop();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'N' && board.board[dx][dy] instanceof WhiteQueen) {
                board.board[dx][dy] = new WhiteKnight();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'R' && board.board[dx][dy] instanceof WhiteQueen) {
                board.board[dx][dy] = new WhiteRock();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'Q' && board.board[dx][dy] instanceof BlackQueen) {
                board.board[dx][dy] = new BlackQueen();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'B' && board.board[dx][dy] instanceof BlackQueen) {
                board.board[dx][dy] = new BlackBishop();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'N' && board.board[dx][dy] instanceof BlackQueen) {
                board.board[dx][dy] = new BlackKnight();
                Board.graphBoard(board);
                return true;
            } else if ((int) input.charAt(6) == 'R' && board.board[dx][dy] instanceof BlackQueen) {
                board.board[dx][dy] = new BlackRock();
                Board.graphBoard(board);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }

    /**
     * Move method
     * <p>
     * This method is used to check whether a move is legal or illegal, it takes the input the board
     * and the a check status. it calls the independent move from each ChessPiece. if the returned
     * board show that the piece was moved then it returns true, otherwise false.
     * <p>
     *
     * @param input  gets the input from the startGame,
     * @param board  gets the board in which the game is being played on
     * @param status determines if the move is under check;
     * @return return true if the move is possible, false if it is not possible.
     */

    static boolean Move(String input, Board board, String status) {
        int ix = 8 - ((int) input.charAt(1) - 48);
        int iy = (int) input.charAt(0) - 97;

        int dx = 8 - ((int) input.charAt(4) - 48);
        int dy = (int) input.charAt(3) - 97;

		  /*  only move king under check
           *  if(status=="check"){
			   if(!(board.board[ix][iy]instanceof King)){
				   return false;
			   }			   
		   }*/
        if (input.length() == 7 && (int) input.charAt(5) == ' ' && ((int) input.charAt(6) == 'N' || (int) input.charAt(6) == 'R' || (int) input.charAt(6) == 'B'
                || (int) input.charAt(6) == 'Q')) {

            return promoPawn(input, board, ix, iy, dx, dy);

        } else {

            board = board.board[ix][iy].move(dx, dy, board);
            if (board.board[ix][iy] == null) {
                Board.graphBoard(board);
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * CheckInput method
     * <p>
     * This method checks whether that input entered by the user is valid or invalid.
     * it also checks if the user imputed draw and if the other user responds for a draw.
     * it also gets the input if the user wants to promote a pawn to an specific piece.
     * <p>
     *
     * @param input  gets the input from the startGame,
     * @param board  gets the board in which the game is being played on
     * @param player determines if the move is under check;
     * @return return true if the input form the user is correct in the program context
     */

    static boolean checkInput(String input, Board board, String player) {

        if (input.length() == 11) {
            String temp = input.substring(6);
            System.out.println(temp);
            if (temp.equals("draw?")) {
                System.out.println("black's move:");
                @SuppressWarnings("resource")
                Scanner sc = new Scanner(System.in);
                String answ = sc.nextLine();
                if (answ.equals("draw")) {
                    System.out.println("Game ended in a draw");
                    System.exit(0);
                } else {
                    return false;
                }

            }
        } else if (input.length() == 7) {

            return (int) input.charAt(5) == ' ' && ((int) input.charAt(6) == 'N' || (int) input.charAt(6) == 'R' || (int) input.charAt(6) == 'B'
                    || (int) input.charAt(6) == 'Q') && ((int) input.charAt(4) == '1' || (int) input.charAt(4) == '8')
                    && board.board[8 - ((int) input.charAt(1) - 48)][(int) input.charAt(0) - 97] instanceof Pawn;

        }


        //REGUALR IMPUT

        int ix = 8 - ((int) input.charAt(1) - 48);
        int iy = (int) input.charAt(0) - 97;

        if (input.length() != 5) {
            return false;
        } else if ((int) input.charAt(0) - 97 < 0 || (int) input.charAt(0) - 97 > 8) {
            return false;
        } else if ((int) input.charAt(3) - 97 < 0 || (int) input.charAt(3) - 97 > 8) {
            return false;
        } else if ((int) input.charAt(1) - 48 < 0 || (int) input.charAt(1) - 48 > 8) {
            return false;
        } else if ((int) input.charAt(4) - 48 < 0 || (int) input.charAt(4) - 48 > 8) {
            return false;
        } else if (input.charAt(2) != ' ') {
            return false;
        } else if (board.board[ix][iy] == null) {
            return false;
        } else if (Objects.equals(player, "white") && !Objects.equals(board.board[ix][iy].getPlayer(), "white")) {
            System.out.print("move a white piece - ");
            return false;
        } else if (Objects.equals(player, "black") && !Objects.equals(board.board[ix][iy].getPlayer(), "black")) {
            System.out.print("move a black piece - ");
            return false;
        } else {
            return true;
        }

    }


    /**
     * StartGame method
     * <p>
     * This method starts the game and have all the conditions for a winner, and loops between
     * players. it gets the user input.
     * <p>
     *
     * @param board gets the board created in the main function.
     */

    @SuppressWarnings("resource")
    static void startGame(Board board) throws FileNotFoundException {
        boolean goodInput;
        boolean winner = false;
        int i = 1;
        String input;
        String player;
        @SuppressWarnings("unused")
        int turn = 0;
        String status = null;
        //Scanner sc = new Scanner(new File("input.txt"));
        Scanner sc = new Scanner(System.in);
        while (!winner) {
            goodInput = false;
            player = null;

            while (!goodInput) {
                if (i % 2 != 0) {
                    System.out.print("White's move: ");
                    player = "white";
                    turn++;
                } else {
                    System.out.print("Black's move: ");
                    player = "black";


                }


                input = sc.nextLine();
                //System.out.println(input);


                if (input.equals("resign")) {
                    System.out.println("End of Game");
                    return;
                }
                goodInput = checkInput(input, board, player);
                if (goodInput) {
                    boolean goodMove = Move(input, board, status);
                    //System.out.println("turn="+turn);

                    if (!goodMove) {

                        System.out.println("Illegal move, try again");
                        goodInput = false;
                        turn--;
                    } else {
                        //check for stalemate
                        if (player.equals("white")) {
                            if (check(board, player)) {
                                System.out.println("check");

                                status = "check";
                                boolean cMate = checkmate(board, player);
                                if (cMate) {
                                    System.out.println("checkmate");
                                    System.out.println("white wins");
                                    winner = true;
                                }
                            } else {
                                status = null;
                            }
                        } else {
                            if (check(board, player)) {
                                System.out.println("check");
                                status = "check";
                                boolean cMate = checkmate(board, player);
                                if (cMate) {
                                    System.out.println("checkmate");
                                    System.out.println("Black wins");
                                    winner = true;
                                }
                            } else {
                                status = null;
                            }
                        }
                    }

                } else {
                    System.out.println("bad input enter again");
                }
            }

            i++;
        }

    }

    /**
     * Check method
     * <p>
     * This method checks if the kings of the opposite player has been exposed to a Check.
     * <p>
     *
     * @param board  gets the board in which the game is being played on
     * @param player A string that stores the word Black or white. stating the player�s current move.
     * @return return true if the king of the opposite player has been exposed to a check.
     */

    static boolean check(Board board, String player) {
        String isDangered;
        String kl = KingLocation(board, player);

        isDangered = isDangered(board, kl.charAt(0) - 48, kl.charAt(1) - 48);
        if (isDangered.equals("false")) {

            return false;
        } else {

            boolean cbk = canBeKilled(board, isDangered.charAt(0) - 48, isDangered.charAt(1) - 48);
            if (cbk) {
                System.out.println("check");
                return false;
            }
            return true;
        }
    }

    /**
     * KingLocation method
     * <p>
     * This method gets a board and a player, and looks for the opossite player�s king location
     * <p>
     *
     * @param board  gets the board in which the game is being played on
     * @param player determine if the player is white or black
     * @return A string that contains the position of the king opposite player in the form of ex: "a5".
     */

    static String KingLocation(Board board, String player) {
        int x = 0;
        int y = 0;

        if (player.equals("white")) {
            for (int i = 0; i < board.board.length; i++) {
                for (int j = 0; j < board.board[0].length; j++) {
                    if (board.board[i][j] instanceof BlackKing) {
                        x = i;
                        y = j;
                    }
                }
            }
            return x + "" + y;

        } else {
            for (int i = 0; i < board.board.length; i++) {
                for (int j = 0; j < board.board[0].length; j++) {
                    if (board.board[i][j] instanceof WhiteKing) {
                        x = i;
                        y = j;
                    }
                }
            }

            return x + "" + y;
        }
    }

    /**
     * EmpDanger method
     * <p>
     * This method checks if an empty adjacent location to a king is in danger, which it is used to
     * determine check mate.
     * <p>
     *
     * @param board gets the board in which the game is being played on
     * @param x     x location of empty slot;
     * @param y     y location of empty slot;
     * @param kx    location of king;
     * @param ky    location of king;
     * @return return true if the empty slot is exposed to another rival piece.
     */

    public static boolean EmpDanger(Board board, int x, int y, int kx, int ky) {
        for (int i = 0; i < board.board.length; i++) {
            for (int j = 0; j < board.board[0].length; j++) {
                if (board.board[i][j] != null && board.board[x][y] == null &&
                        (!Objects.equals(board.board[i][j].getPlayer(), board.board[kx][ky].getPlayer()))) {

                    Board temp = board.board[i][j].move(x, y, board);

                    if (temp.board[i][j] == null) {
                        board.board[i][j] = board.board[x][y];
                        board.board[x][y] = null;
                        //System.out.println("in danger:"+x+""+y+"is dangerde by"+i+""+j);

                        return true;
                    }
                } else if (board.board[i][j] != null && board.board[x][y] == null &&
                        (Objects.equals(board.board[i][j].getPlayer(), board.board[kx][ky].getPlayer())) &&
                        board.board[kx][ky] != board.board[i][j]) {

                    Board temp = board.board[i][j].move(x, y, board);

                    if (temp.board[i][j] == null) {
                        board.board[i][j] = board.board[x][y];
                        board.board[x][y] = null;
                        //System.out.println("can close check"+board.board[i][j].write());

                        return false;
                    }
                }
            }
        }
        return false;

    }

    /**
     * canBeKilled method
     * <p>
     * This method checks if a piece that is checking the rivla king can be killed so
     * avoiding checkmate check
     * <p>
     *
     * @param board gets the board in which the game is being played on
     * @param x     location of king;
     * @param y     location of king;
     * @return return true if the piece that is checking the kind can be killed
     */

    static boolean canBeKilled(Board board, int x, int y) {
        for (int i = 0; i < board.board.length; i++) {
            for (int j = 0; j < board.board[0].length; j++) {
                if (board.board[i][j] != null && (!(board.board[i][j].getPlayer().equals(board.board[x][y].getPlayer())))) {
                    ChessPiece t = board.board[x][y];
                    Board temp = board.board[i][j].move(x, y, board);
                    if (temp.board[i][j] == null) {
                        board.board[i][j] = board.board[x][y];
                        board.board[x][y] = t;
                        //System.out.println(x+""+y+"can be killed by"+i+""+j);
                        return true;
                    }
                }
            }
        }
        return false;


    }

    /**
     * isDangered method
     * <p>
     * This method checks if a king is in danger, which it is used to
     * determine check.
     * <p>
     *
     * @param board gets the board in which the game is being played on
     * @param x     location of king;
     * @param y     location of king;
     * @return return true if the location where the king is located is exposed to a rival piece
     */

    static String isDangered(Board board, int x, int y) {

        for (int i = 0; i < board.board.length; i++) {
            for (int j = 0; j < board.board[0].length; j++) {
                if (board.board[i][j] != null && (!(board.board[i][j].getPlayer().equals(board.board[x][y].getPlayer())))) {
                    Board temp = board.board[i][j].move(x, y, board);
                    if (temp.board[i][j] == null) {
                        board.board[i][j] = board.board[x][y];
                        if (board.board[x][y].getPlayer().equals("black")) {
                            board.board[x][y] = new WhiteKing();
                            board.board[x][y].x = x;
                            board.board[x][y].y = y;
                        } else {
                            board.board[x][y] = new BlackKing();
                            board.board[x][y].x = x;
                            board.board[x][y].y = y;
                        }
                        //System.out.println(x+""+y+"is dangerde by"+i+""+j);
                        return i + "" + j;
                    }
                }
            }
        }
        return "false";
    }

    /**
     * This method looks for all the slots that are around a king when it is in check
     *
     * @param board  gets the board in which the game is being played on.
     * @param player String stating if the player is white or black.
     * @return true if there is a checkmate.
     */
    static boolean checkmate(Board board, String player) {
        String location = KingLocation(board, player);
        int x = (int) location.charAt(0) - 48;
        int y = (int) location.charAt(1) - 48;

        if (x == 0 && y == 0) {
            if (board.board[x][y + 1] == null && (!EmpDanger(board, x, y + 1, 0, 0))) {
                return false;
            } else if (board.board[x + 1][y + 1] == null && (!EmpDanger(board, x + 1, y + 1, 0, 0))) {
                return false;
            } else if (board.board[x + 1][y] == null && (!EmpDanger(board, x + 1, y, 0, 0))) {
                return false;
            } else {
                return true;
            }

        } else if (x == 0 && y == 7) {
            if (board.board[x][y - 1] == null && (!EmpDanger(board, x, y - 1, 0, 7))) {
                return false;
            } else if (board.board[x + 1][y] == null && (!EmpDanger(board, x + 1, y, 0, 7))) {
                return false;
            } else if (board.board[x + 1][y - 1] == null && (!EmpDanger(board, x + 1, y - 1, 0, 7))) {
                return false;
            } else {
                return true;
            }


        } else if (x == 7 && y == 7) {
            if (board.board[x][y - 1] == null && (!EmpDanger(board, x, y - 1, 7, 7))) {
                return false;
            } else if (board.board[x - 1][y - 1] == null && (!EmpDanger(board, x - 1, y - 1, 7, 7))) {
                return false;
            } else if (board.board[x - 1][y] == null && (!EmpDanger(board, x - 1, y, 7, 7))) {
                return false;
            } else {
                return true;
            }


        } else if (x == 7 && y == 0) {
            if (board.board[x - 1][y] == null && (!EmpDanger(board, x - 1, y, 7, 0))) {
                return false;
            } else if (board.board[x - 1][y + 1] == null && (!EmpDanger(board, x - 1, y + 1, 7, 0))) {
                return false;
            } else if (board.board[x][y + 1] == null && (!EmpDanger(board, x, y + 1, 7, 0))) {
                return false;
            } else {
                return true;
            }


        } else if (x == 0 && (y > 0 && y < 7)) {
            System.out.println("..");

            if (board.board[x][y - 1] == null && (!EmpDanger(board, x, y - 1, x, y))) {
                return false;
            } else if (board.board[x][y + 1] == null && (!EmpDanger(board, x, y + 1, x, y))) {
                return false;
            } else if (board.board[x + 1][y + 1] == null && (!EmpDanger(board, x + 1, y + 1, x, y))) {
                return false;
            } else if (board.board[x + 1][y] == null && (!EmpDanger(board, x + 1, y, x, y))) {
                return false;
            } else return !(board.board[x + 1][y - 1] == null && (!EmpDanger(board, x + 1, y - 1, x, y)));
        } else if ((x > 0 && x < 7) && y == 7) {
            if (board.board[x][y - 1] == null && (!EmpDanger(board, x, y - 1, x, y))) {
                return false;
            } else if (board.board[x - 1][y - 1] == null && (!EmpDanger(board, x - 1, y - 1, x, y))) {
                return false;
            } else if (board.board[x - 1][y] == null && (!EmpDanger(board, x - 1, y, x, y))) {
                return false;
            } else if (board.board[x + 1][y] == null && (!EmpDanger(board, x + 1, y, x, y))) {
                return false;
            } else return !(board.board[x + 1][y - 1] == null && (!EmpDanger(board, x + 1, y - 1, x, y)));

        } else if (x == 7 && (y > 0 && y < 7)) {
            if (board.board[x][y - 1] == null && (!EmpDanger(board, x, y - 1, x, y))) {
                return false;
            } else if (board.board[x - 1][y - 1] == null && (!EmpDanger(board, x - 1, y - 1, x, y))) {
                return false;
            } else if (board.board[x - 1][y] == null && (!EmpDanger(board, x - 1, y, x, y))) {
                return false;
            } else if (board.board[x - 1][y + 1] == null && (!EmpDanger(board, x - 1, y + 1, x, y))) {
                return false;
            } else return !(board.board[x][y + 1] == null && (!EmpDanger(board, x, y + 1, x, y)));


        } else if ((x > 0 && x < 7) && y == 0) {
            if (board.board[x - 1][y] == null && (!EmpDanger(board, x - 1, y, x, y))) {
                return false;
            } else if (board.board[x - 1][y + 1] == null && (!EmpDanger(board, x - 1, y + 1, x, y))) {
                return false;
            } else if (board.board[x][y + 1] == null && (!EmpDanger(board, x, y + 1, x, y))) {
                return false;
            } else if (board.board[x + 1][y + 1] == null && (!EmpDanger(board, x + 1, y + 1, x, y))) {
                return false;
            } else return !(board.board[x + 1][y] == null && (!EmpDanger(board, x + 1, y, x, y)));

        } else {

            if (board.board[x][y - 1] == null && (!EmpDanger(board, x, y - 1, x, y))) {
                return false;
            } else if (board.board[x - 1][y - 1] == null && (!EmpDanger(board, x - 1, y - 1, x, y))) {
                return false;
            } else if (board.board[x - 1][y] == null && (!EmpDanger(board, x - 1, y, x, y))) {
                return false;
            } else if (board.board[x - 1][y + 1] == null && (!EmpDanger(board, x - 1, y + 1, x, y))) {
                return false;
            } else if (board.board[x][y + 1] == null && (!EmpDanger(board, x, y + 1, x, y))) {
                return false;
            } else if (board.board[x + 1][y + 1] == null && (!EmpDanger(board, x + 1, y + 1, x, y))) {
                return false;
            } else if (board.board[x + 1][y] == null && (!EmpDanger(board, x + 1, y, x, y))) {
                return false;
            } else return !(board.board[x + 1][y - 1] == null && (!EmpDanger(board, x + 1, y - 1, x, y)));
        }

    }

    public static void main(String[] args) throws FileNotFoundException {

        System.out.println("Welcome to the chess game");
        Board board = new Board();

        Board.graphBoard(board);

        startGame(board);
    }
}
